<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmbedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('embeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('thumbnail')->nullable();
            $table->text('titre');
            $table->string('auteur')->nullable();
            $table->longText('description')->nullable();
            $table->text('duree')->nullable();
            $table->integer('nb_view')->nullable();
            $table->integer('categorie_id')->nullable();
            $table->boolean('etat')->nullable();
            $table->integer('visibilite_id')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('embeds');
    }
}
