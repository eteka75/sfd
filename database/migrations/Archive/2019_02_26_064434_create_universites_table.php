<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUniversitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable();
            $table->string('image_cover')->nullable();
            $table->string('nom');
            $table->string('sigle');
            $table->integer('pays_id')->nullable();
            $table->text('description')->nullable();
            $table->longText('contenu')->nullable();
            $table->boolean('etat')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('universites');
    }
}
