<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url_fichier')->nullable();
            $table->string('image_pub')->nullable();
            $table->text('titre')->nullable();
            $table->string('auteurs')->nullable();
            $table->longText('description')->nullable();
            $table->integer('annee_pub')->nullable();
            $table->integer('discipline_id')->nullable();
            $table->string('journal')->nullable();
            $table->string('categorie_journal')->nullable();
            $table->text('impactfactor')->nullable();
            $table->string('volume')->nullable();
            $table->string('numero')->nullable();
            $table->text('resume')->nullable();
            $table->string('mots_cle')->nullable();
            $table->string('pages')->nullable();
            $table->integer('centre_id')->nullable();
            $table->integer('type_document_id')->nullable();
            $table->boolean('etat')->nullable();
            $table->integer('visibilite_id')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publications');
    }
}
