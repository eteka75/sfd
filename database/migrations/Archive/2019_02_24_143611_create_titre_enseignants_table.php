<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTitreEnseignantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titre_enseignants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('sigle')->nullable();
            $table->text('description')->nullable();
            $table->boolean('etat')->nullable();
            $table->index(['id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('titre_enseignants');
    }
}
