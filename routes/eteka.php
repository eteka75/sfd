<?php
//
Auth::routes();
Route::get('/',"PageController@getIndex");
Route::get('/admin',"AdminController@index")->name('admin.index');;
Route::get('admin/type',"admin\TypeController@index")->name('type.index');//Affichages de tous les types enrégistrés

Route::get('admin/type/create',"admin\TypeController@create")->name('type.create');;//Affichage du formulaire d'enregistrement
Route::post('admin/type/',"admin\TypeController@store")->name('type.store');//Sauvegarde des sonnées dans la BDD


Route::get('admin/type/{id}',"admin\TypeController@show")->name('type.show');//Affichage des informations supplémentaires sur le Type

Route::get('admin/type/{id}/edit',"admin\TypeController@edit")->name('type.edit');//
Route::patch('admin/type/{id}',"admin\TypeController@update")->name('type.update');;

Route::delete('admin/type/{id}',"admin\TypeController@destroy")->name('type.show');