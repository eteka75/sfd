@extends('layouts.web')
@section('title',$publication?$publication->titre:'')
@section('content')
<div class="baniere" id="baniere_publications">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				@if(isset($publication->type) && !empty($publication->type))
					<span class="label text-whites bg-default">{{$publication->type->nom}}</span>
					@endif
				<h2 class="page_title" title="{{$publication->titre}}">{{$publication->titre}}</h2>
				<ol class="breadcrumb mb30">
	                <li class="breadcrumb-item"><a href="{{url('/')}}">Accueil </a></li>
	                <li class="breadcrumb-item"><a href="{{url('/publications')}}"> Publications</a></li>
	                <li class="breadcrumb-item"> {{$publication->titre}}</li>
	            </ol>
			</div>
		</div>
	</div>
</div> 
@if(isset($publication) && ! empty($publication))
<div class="" id="">
	<div class="container">
		<div class="row">
			
			<div class="col-md-8 col-lg-9 mt30 ">
				<div class="card">
					
					@if(isset($publication->image_pub) && file_exists(public_path().'/'.$publication->image_pub))
					<img src="{{asset($publication->image_pub)}}" class="img-responsive" alt=" {{$publication->titre}}">
					@endif
					<div class="borderd c pad10">
					<div class=" border pad10">
						<?php $auteurs=$publication->auteurs()?$publication->auteurs()->get():NULL;
                                   // dd($auteurs);
                                $cmpt=0;
                                $t=count($auteurs);
                                ?>
                                <ul class="post-meta list-inline">
                                 <li>
                                        <small> <i class="fa fa-calendar-o"></i> &nbsp; <b>Ajouté le</b> {{strftime("%d %B %Y à %HH:%M",strtotime($publication->created_at))}}</small>
                                    </li>
                                    <li>
                                        <small> <i class="fa fa-eye"></i>&nbsp; Vues  ({{$publication->nb_view}})</small>
                                    </li>
                                    <li>
                                        <small> <i class="fa fa-download"></i> &nbsp;Téléchargements ({{$publication->nb_download}})</small>
                                    </li>
                                </ul>
					</div>
	                <div class="bge pad10">	
		                <table  class="table  mt20  table-sm table-stripsed bgcolor">
							<tbody>
								<tr>
									<th width="20%" class="text-right bge" valign="top">
										Auteur{{$auteurs->count()>1?'s':''}} :
									</th>
									<td class=""  valign="top">
										<ul class="list-inline text-sm categoriess">
										<?php $nc=1 ; ?>	
										 @foreach($auteurs as $a)
						                    <li class="text-xs">&nbsp;&nbsp;
						                     <a class="fnormal " href="{{route('profile',$a->profile)}}"><i class="fa fa-user-circle"></i> &nbsp;{{$a->prenom.' '.$a->nom}}</a>
						                      <?php 
						                      $d=$nc;
						                      echo ($nc++>0 && $nc<=$auteurs->count())?'&nbsp;&nbsp;| &nbsp;&nbsp;':''; ?>
						                    </li>
						                    @endforeach
								        </ul>
									</td>
								</tr>
								<tr>
									<th width="40%" class="text-right bge">
										Année de publication :
									</th>
									<td>
										<a href="{{url('annee/'.$publication->annee_pub)}}">{{$publication->annee_pub}}</a>
									</td>
								</tr>
								<?php 
										$auteurs=$publication->auteurs()?$publication->auteurs()->get():NULL;
										 $terminaison=count($auteurs)>1?'s':'';
										?>
								<tr>
									<th width="40%" class="text-right bge" valign="top">
										Laboratoire{{$terminaison}} : 
									</th>
									<td>
										<?php 
										$auteurs=$publication->auteurs()?$publication->auteurs()->get():NULL;
										?>
										<ul class="m0 list-unstyled">
											<?php
						                        $tabid_affiliation=[];
						                    ?>
						                    @foreach($auteurs as $a)
						                        @if($a->affiliation() && $a->affiliation->id)                        
						                            @if(!in_array($a->affiliation->id,$tabid_affiliation))
						                                <?php
						                                    $tabid_affiliation[]=$a->affiliation->id;
						                                ?>
						                                <span class="label bg-blue rond0 "> <a class=" text-white" href="{{route('labo',strtolower($a->affiliation->sigle))}}">{{$a->affiliation->nom}}</a></span>
						                                <?php ($cmpt<$t)?'&nbsp;&nbsp;|&nbsp;&nbsp;':'' ?>
						                            @endif
						                        @endif
						                    @endforeach
										</ul>
									</td>
								</tr>
								@if($publication->discipline)
								<tr>
									<th width="40%" class="text-right bge">
										Discipline : 
									</th>
									<td>
										<a href="{{route('getDiscipline',strtolower($publication->discipline->code))}}">{{$publication->discipline->nom}}</a>
									</td>
								</tr>
								@endif
								@if(!empty($publication->pages))
								<tr>
									<th width="40%" class="text-right bge">
										Références :
									</th>
									<td>
										{{$publication->pages}}
									</td>
								</tr>
								@endif
								@if(!empty($publication->journal))
								<tr>
									<th width="40%" class="text-right bge">
										Journal :
									</th>
									<td>
										{{$publication->journal}}
									</td>
								</tr>
								@endif
								<!--tr>
									<th width="40%" class="text-right bge" valign="top">
										Impact factors :
									</th>
									<td valign="top">
										<div class="text-xs pad10 card">	
												{{$publication->impactfactor}}
										</div>
									</td>
								</tr-->
								@if(!empty($publication->mots_cle))
								<tr>
									<th width="40%" class="text-right bge">
										Mots clés :
									</th>
									<td>
										<?php
											$tab_mc=explode(',', $publication->mots_cle);
											
										?>
										<ul class="list-inline list-badge">
										@foreach($tab_mc as $mc)
											<li><a href="{{route('getTage',strtolower(trim($mc)))}}">{{$mc}}</a></li>
										@endforeach
										</ul>
									</td>
								</tr>
								@endif
								@if($publication->numero)
								<tr>
									<th width="40%" class="text-right bge">
										Numéro :
									</th>
									<td>
										{{$publication->numero}}
									</td>
								</tr>
								@endif
								@if(!empty($publication->impactfactor))
								<tr>
									<th width="40%" class="text-right bge">
										Statut de la révue :
									</th>
									<td>
										<span class=" borderctext-bold text-success"><i class="fa fa-check "></i> {{$publication->impactfactor}}&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								@endif
								<?php
								$fileSearch=public_path().'/'.$publication->url_fichier;
								$taille=filesize($fileSearch);
								
								?>
        	 				@if(!empty($publication->url_fichier) && file_exists($fileSearch))
								<tr>
									<th width="40%" class="text-right bge">
										Fichier :
									</th>

									<td>
										<a href="{{route('downloadPDF',$publication->slug)}}"><img src="{{asset('assets/images/pdf.png')}}" class="pst10pb10ml10" alt="PDF" height="20px">Télécharger la version PDF
										( {{$publication->getFileSize()}} )
										</a>
										<!--a href="{{url('download/publication/'.$publication->slug)}}"><img src="{{asset('assets/images/pdf.png')}}" class="pst10pb10ml10" alt="PDF" height="20px">Télécharger la version PDF</a-->
									</td>
								</tr>
								@endif
							</tbody>
						</table>
					</div>
			</div>
			</div>
			@if(!empty($publication->resume))
			<div class="card_main mt10 mb10">				
							
				<div class="bg-white pt10 pb30">
					<h3 class="sidebar-title sidebar-title2">Résumé</h3>
					<blockquote>{{$publication->resume}}</blockquote>
				</div> 
			</div>
			@endif
			
			
			@if(!empty($publication->description))
			<div class="card_main mt10 mb10">
				<h3 class="sidebar-title sidebar-title2">Informations</h3>
				<div class="well">
						{{$publication->description}}
				</div>
			</div>
			@endif
			</div>
			@include('partials.right2')
		</div>
	</div>


</div> 
@endif
@endsection