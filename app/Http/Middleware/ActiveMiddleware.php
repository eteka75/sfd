<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use \App\User ;
use \App\Models\Publication as Article;
use \App\Models\CentreDeRecherche;
use \App\Models\Discipline;
use \App\Models\Publication;
use \App\Models\TypeDocument;
use Auth;
use View;

class ActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            if(Auth::user()->etat!=1){
                Session::flash('warning', "Votre compte n'est pas activé. <a href=".route("getActivataion",Auth::user()->profile).">Cliquez ici</a> pour saisir le code d'activation. <br>Pour plus de renseignement, veuillez contacter l'administratueur de la Plateforme ");
                    //dd(Auth::user());
                //return redirect()->intended('/repertoire');
            }
        } else{
             return redirect("login"); 
        }  
        return $next($request);

       
    }
}
