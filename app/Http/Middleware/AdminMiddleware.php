<?php

namespace App\Http\Middleware;

use Closure;
use \App\User ;
use \App\Models\Publication as Article;
use \App\Models\CentreDeRecherche;
use \App\Models\Discipline;
use \App\Models\Publication;
use \App\Models\TypeDocument;
use Auth;
use View;

class AdminMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $select_disciplines=Discipline::pluck('nom','id')->prepend('Tout','all');
        View::share(compact('select_disciplines','type_pubs'));
       return $next($request);
    }
}
