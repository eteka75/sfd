@extends('layouts.web')

@section('content')
    <div class="container">
             
            <div class="row">
            
                    <div class="col-md-3">   
            @include('admin.menu')
            </div>
            <div class="col-md-9">
                        <div class="content_main rond3">
                            <div class="panel panel-default">
                                <div class="panel-heading">Type #{{ $type->id }}</div>
                                <div class="panel-body">

                                    <a href="{{ url('/admin/type') }}" title="Back"><button class="btn btn-warning btn-xs mt20"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                                    <a href="{{ url('/admin/type/' . $type->id . '/edit') }}" title="Edit Type"><button class="btn btn-primary btn-xs mt20"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['admin/type', $type->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs mt20',
                                                'title' => 'Suppression Type',
                                                'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                        ))!!}
                                    {!! Form::close() !!}
                                    <br/>
                                    <br/>

                                    <div class="table-responsive">
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <th>ID</th><td>{{ $type->id }}</td>
                                                </tr>
                                                <tr><th> {{ trans('type.nom') }} </th><td> {{ $type->nom }} </td></tr><tr><th> {{ trans('type.date_type') }} </th><td> {{ $type->date_type }} </td></tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                     </div>
            </div>
        </div>
    </div>
@endsection
