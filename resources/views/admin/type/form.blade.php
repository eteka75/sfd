<div class="form-group row {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('type.nom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('nom', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('date_type') ? 'has-error' : ''}}">
    {!! Form::label('date_type', trans('type.date_type'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::date('date_type', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('date_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
