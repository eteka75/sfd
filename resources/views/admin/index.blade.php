@extends('layouts.admin')
@section('content')
<!-- Content Body -->

	<h1 class="h2 mb-2">Tableau de board</h1>

	<!-- Breadcrumb -->
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.html">Accueil</a></li>
			<li class="breadcrumb-item active" aria-current="page">Tableau de board</li>
		</ol>
	</nav>
	<!-- End Breadcrumb -->

	<div class="row">
		<div class="col-md-6 mb-5">
			<form class="h-100" action="https://htmlstream.com/">
				<!-- Card -->
				<div class="card h-100">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Form Controls</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<!-- Text -->
						<div class="form-group">
							<label for="exampleInputText1">Default input</label>
							<input id="exampleInputText1" class="form-control" type="text" placeholder="Placeholder">
						</div>
						<!-- End Text -->

						<hr class="my-4">

						<!-- Input with Left Icon -->
						<div class="form-group">
							<label for="exampleInputText2">Input with left icon</label>

							<div class="input-group">
								<div class="input-group-prepend">
								<span class="input-group-text">
									<span class="ti-user"></span>
								</span>
								</div>

								<input id="exampleInputText2" class="form-control" type="text" placeholder="Placeholder">
							</div>
						</div>
						<!-- End Input with Left Icon -->

						<!-- Input with Right Icon -->
						<div class="form-group">
							<label for="exampleInputText3">Input with right icon</label>

							<div class="input-group">
								<input id="exampleInputText3" class="form-control" type="text" placeholder="Placeholder">

								<div class="input-group-append">
								<span class="input-group-text">
									<span class="ti-user"></span>
								</span>
								</div>
							</div>
						</div>
						<!-- End Input with Right Icon -->

						<hr class="my-4">

						<!-- Disabled Text -->
						<div class="form-group">
							<label for="exampleInputText4">Disabled</label>
							<input id="exampleInputText4" class="form-control" type="text" placeholder="Placeholder" disabled>
							<small class="form-text text-muted">Text feedback.</small>
						</div>
						<!-- End Disabled Text -->

						<!-- Success Text -->
						<div class="form-group">
							<label for="exampleInputText5">Success</label>
							<input id="exampleInputText5" class="form-control is-valid" type="text" placeholder="Placeholder">
							<div class="valid-feedback">Success feedback.</div>
						</div>
						<!-- End Success Text -->

						<!-- Error Text -->
						<div class="form-group">
							<label for="exampleInputText6">Error</label>
							<input id="exampleInputText6" class="form-control is-invalid" type="text" placeholder="Placeholder">
							<div class="invalid-feedback">Error feedback.</div>
						</div>
						<!-- End Error Text -->
					</div>
					<!-- End Card Body -->
				</div>
				<!-- End Card -->
			</form>
		</div>

		<div class="col-md-6 mb-5">
			<form class="h-100" action="https://htmlstream.com/">
				<!-- Card -->
				<div class="card h-100">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Piled Form Controls</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<!-- Text -->
						<div class="form-group">
							<label for="exampleInputText2_1">Default input</label>
							<input id="exampleInputText2_1" class="form-control form-pill" type="text" placeholder="Placeholder">
						</div>
						<!-- End Text -->

						<hr class="my-4">

						<!-- Input with Left Icon -->
						<div class="form-group">
							<label for="exampleInputText2_2">Input with left icon</label>

							<div class="input-group">
								<div class="input-group-prepend">
								<span class="input-group-text">
									<span class="ti-user"></span>
								</span>
								</div>

								<input id="exampleInputText2_2" class="form-control form-pill" type="text" placeholder="Placeholder">
							</div>
						</div>
						<!-- End Input with Left Icon -->

						<!-- Input with Right Icon -->
						<div class="form-group">
							<label for="exampleInputText2_3">Input with right icon</label>

							<div class="input-group">
								<input id="exampleInputText2_3" class="form-control form-pill" type="text" placeholder="Placeholder">

								<div class="input-group-append">
								<span class="input-group-text">
									<span class="ti-user"></span>
								</span>
								</div>
							</div>
						</div>
						<!-- End Input with Right Icon -->

						<hr class="my-4">

						<!-- Disabled Text -->
						<div class="form-group">
							<label for="exampleInputText2_4">Disabled</label>
							<input id="exampleInputText2_4" class="form-control form-pill" type="text" placeholder="Placeholder" disabled>
							<small class="form-text text-muted">Text feedback.</small>
						</div>
						<!-- End Disabled Text -->

						<!-- Success Text -->
						<div class="form-group">
							<label for="exampleInputText2_5">Success</label>
							<input id="exampleInputText2_5" class="form-control form-pill is-valid" type="text" placeholder="Placeholder">
							<div class="valid-feedback">Success feedback.</div>
						</div>
						<!-- End Success Text -->

						<!-- Error Text -->
						<div class="form-group">
							<label for="exampleInputText2_6">Error</label>
							<input id="exampleInputText2_6" class="form-control form-pill is-invalid" type="text" placeholder="Placeholder">
							<div class="invalid-feedback">Error feedback.</div>
						</div>
						<!-- End Error Text -->
					</div>
					<!-- End Card Body -->
				</div>
				<!-- End Card -->
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 mb-5">
			<form action="https://htmlstream.com/">
				<!-- Card -->
				<div class="card">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Checkboxes</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<!-- Checkbox -->
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input id="exampleCheck4" class="custom-control-input" type="checkbox">
								<label class="custom-control-label" for="exampleCheck4">Unchecked</label>
							</div>
						</div>
						<!-- End Checkbox -->

						<!-- Checkbox Checked -->
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input id="exampleCheck5" class="custom-control-input" type="checkbox" checked>
								<label class="custom-control-label" for="exampleCheck5">Checked</label>
							</div>
						</div>
						<!-- End Checkbox Checked -->

						<hr class="my-4">

						<!-- Checkbox -->
						<div class="form-group disabled">
							<div class="custom-control custom-checkbox">
								<input id="exampleCheck8" class="custom-control-input" type="checkbox" disabled>
								<label class="custom-control-label" for="exampleCheck8">Disabled unchecked</label>
							</div>
						</div>
						<!-- End Checkbox -->

						<!-- Checkbox Checked -->
						<div class="form-group disabled">
							<div class="custom-control custom-checkbox">
								<input id="exampleCheck9" class="custom-control-input" type="checkbox" checked disabled>
								<label class="custom-control-label" for="exampleCheck9">Disabled checked</label>
							</div>
						</div>
						<!-- End Checkbox Checked -->

						<hr class="my-4">

						<!-- Checkbox Bordered, Checked -->
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input id="exampleCheck12" class="custom-control-input is-invalid" type="checkbox">
								<label class="custom-control-label" for="exampleCheck12">Unchecked error</label>
							</div>
						</div>
						<!-- End Checkbox Bordered, Checked -->

						<!-- Checkbox Checked -->
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<input id="exampleCheck13" class="custom-control-input is-valid" type="checkbox" checked>
								<label class="custom-control-label" for="exampleCheck13">Checked success</label>
							</div>
						</div>
						<!-- End Checkbox Checked -->
					</div>
					<!-- End Card Body -->
				</div>
				<!-- End Card -->
			</form>
		</div>

		<div class="col-md-6 mb-5">
			<form action="https://htmlstream.com/">
				<!-- Card -->
				<div class="card">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Radio</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<!-- Checkbox -->
						<div class="form-group">
							<div class="custom-control custom-radio">
								<input id="exampleRadio4" class="custom-control-input" type="radio">
								<label class="custom-control-label" for="exampleRadio4">Unchecked</label>
							</div>
						</div>
						<!-- End Checkbox -->

						<!-- Checkbox Checked -->
						<div class="form-group">
							<div class="custom-control custom-radio">
								<input id="exampleRadio5" class="custom-control-input" type="radio" checked>
								<label class="custom-control-label" for="exampleRadio5">Checked</label>
							</div>
						</div>
						<!-- End Checkbox Checked -->

						<hr class="my-4">

						<!-- Radio -->
						<div class="form-group disabled">
							<div class="custom-control custom-radio">
								<input id="exampleRadio8" class="custom-control-input" type="radio" disabled>
								<label class="custom-control-label" for="exampleRadio8">Disabled unchecked</label>
							</div>
						</div>
						<!-- End Radio -->

						<!-- Radio Checked -->
						<div class="form-group disabled">
							<div class="custom-control custom-radio">
								<input id="exampleRadio9" class="custom-control-input" type="radio" checked disabled>
								<label class="custom-control-label" for="exampleRadio9">Disabled checked</label>
							</div>
						</div>
						<!-- End Radio Checked -->

						<hr class="my-4">

						<!-- Radio Bordered, Checked -->
						<div class="form-group">
							<div class="custom-control custom-radio">
								<input id="exampleRadio12" class="custom-control-input is-invalid" type="radio">
								<label class="custom-control-label" for="exampleRadio12">Unchecked error</label>
							</div>
						</div>
						<!-- End Radio Bordered, Checked -->

						<!-- Radio Checked -->
						<div class="form-group">
							<div class="custom-control custom-radio">
								<input id="exampleRadio13" class="custom-control-input is-valid" type="radio" checked>
								<label class="custom-control-label" for="exampleRadio13">Checked success</label>
							</div>
						</div>
						<!-- End Radio Checked -->
					</div>
					<!-- End Card Body -->
				</div>
				<!-- End Card -->
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 mb-5 mb-md-0">
			<!-- Card -->
			<div class="card h-100">
				<!-- Card Header -->
				<header class="card-header">
					<h2 class="h4 card-header-title">Range</h2>
				</header>
				<!-- End Card Header -->

				<!-- Card Body -->
				<div class="card-body pt-0">
  <label for="customRange1">Default range</label>
  <input type="range" class="custom-range" value="80" id="customRange1">

  <hr class="my-4">

  <label for="customRange2">Range with values for min and max</label>
  <input type="range" class="custom-range" min="0" max="50" value="5" id="customRange2">
				</div>
				<!-- End Card Body -->
			</div>
			<!-- End Card -->
		</div>

		<div class="col-md-6 mb-5 mb-md-0">
			<!-- Card -->
			<div class="card h-100">
				<!-- Card Header -->
				<header class="card-header">
					<h2 class="h4 card-header-title">Switches</h2>
				</header>
				<!-- End Card Header -->

				<!-- Card Body -->
				<div class="card-body pt-0">
  <div class="custom-control custom-switch mb-2">
    <input type="checkbox" class="custom-control-input" id="customSwitch1-1">
    <label class="custom-control-label" for="customSwitch1-1">Unchecked switch</label>
  </div>

  <div class="custom-control custom-switch mb-2">
    <input type="checkbox" class="custom-control-input" id="customSwitch1-2" checked>
    <label class="custom-control-label" for="customSwitch1-2">Checked switch</label>
  </div>

  <hr class="my-4">

  <div class="custom-control custom-switch mb-2">
    <input type="checkbox" class="custom-control-input" disabled id="customSwitch2-1">
    <label class="custom-control-label" for="customSwitch2-1">Disabled Unchecked</label>
  </div>

  <div class="custom-control custom-switch mb-2">
    <input type="checkbox" class="custom-control-input" disabled checked id="customSwitch2-2">
    <label class="custom-control-label" for="customSwitch2-2">Disabled Checked</label>
  </div>
				</div>
				<!-- End Card Body -->
			</div>
			<!-- End Card -->
		</div>
	</div>
	<!-- End Content Body -->

				

@endsection