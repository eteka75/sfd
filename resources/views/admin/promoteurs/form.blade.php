<div class="form-group row {{ $errors->has('denomination') ? 'has-error' : ''}}">
    {!! Form::label('denomination', trans('promoteur.denomination'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('denomination', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('denomination', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('adresse') ? 'has-error' : ''}}">
    {!! Form::label('adresse', trans('promoteur.adresse'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('adresse', null, ['class' => 'form-control']) !!}
        {!! $errors->first('adresse', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('telephone') ? 'has-error' : ''}}">
    {!! Form::label('telephone', trans('promoteur.telephone'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
        {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', trans('promoteur.email'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('datecreation') ? 'has-error' : ''}}">
    {!! Form::label('datecreation', trans('promoteur.datecreation'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::date('datecreation', null, ['class' => 'form-control']) !!}
        {!! $errors->first('datecreation', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('ville', trans('promoteur.ville'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('ville', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ville', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('assuranceentreprise') ? 'has-error' : ''}}">
    {!! Form::label('assuranceentreprise', trans('promoteur.assuranceentreprise'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::textarea('assuranceentreprise', null, ['class' => 'form-control']) !!}
        {!! $errors->first('assuranceentreprise', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('licence') ? 'has-error' : ''}}">
    {!! Form::label('licence', trans('promoteur.licence'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('licence', null, ['class' => 'form-control']) !!}
        {!! $errors->first('licence', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('type_id') ? 'has-error' : ''}}">
    {!! Form::label('type_id', trans('promoteur.type_id'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::number('type_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('surnom') ? 'has-error' : ''}}">
    {!! Form::label('surnom', trans('promoteur.surnom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('surnom', null, ['class' => 'form-control']) !!}
        {!! $errors->first('surnom', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('promoteur.nom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('nom', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', trans('promoteur.prenom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('prenom', null, ['class' => 'form-control']) !!}
        {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('sexe') ? 'has-error' : ''}}">
    {!! Form::label('sexe', trans('promoteur.sexe'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('sexe', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sexe', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('situationmatrimoniale') ? 'has-error' : ''}}">
    {!! Form::label('situationmatrimoniale', trans('promoteur.situationmatrimoniale'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('situationmatrimoniale', null, ['class' => 'form-control']) !!}
        {!! $errors->first('situationmatrimoniale', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('nompere') ? 'has-error' : ''}}">
    {!! Form::label('nompere', trans('promoteur.nompere'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('nompere', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nompere', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('nommere') ? 'has-error' : ''}}">
    {!! Form::label('nommere', trans('promoteur.nommere'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('nommere', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nommere', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('niveau_etude') ? 'has-error' : ''}}">
    {!! Form::label('niveau_etude', trans('promoteur.niveau_etude'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('niveau_etude', null, ['class' => 'form-control']) !!}
        {!! $errors->first('niveau_etude', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('nb_enfant_a_charge') ? 'has-error' : ''}}">
    {!! Form::label('nb_enfant_a_charge', trans('promoteur.nb_enfant_a_charge'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::number('nb_enfant_a_charge', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nb_enfant_a_charge', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('nb_autrepersone_a_charge') ? 'has-error' : ''}}">
    {!! Form::label('nb_autrepersone_a_charge', trans('promoteur.nb_autrepersone_a_charge'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::number('nb_autrepersone_a_charge', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nb_autrepersone_a_charge', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', trans('promoteur.etat'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        <div class="checkbox">
    <label>{!! Form::radio('etat', '1', true) !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('etat', '0') !!} No</label>
</div>
        {!! $errors->first('etat', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
