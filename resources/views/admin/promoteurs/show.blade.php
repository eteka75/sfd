@extends('layouts.web')

@section('content')
    <div class="container">
             <div class="row">
                <div class="col-sm-12 ">
                    <ol class="breadcrumb mb30">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Accueil </a></li>
                        <li class="breadcrumb-item"><a href="{{url('/administration')}}">Administration </a></li>
                        <li class="breadcrumb-item"> <a href="{{ url('/admin/promoteur/') }}">Promoteur</a></li>
                        <li class="breadcrumb-item"> Promoteur #{{ $promoteur->id }} </li>
                    </ol>
                </div>
            </div>
            <div class="row">
            
                    @include('admin.sidebar')

                    <div class="col-md-10">
                        <div class="content_main rond3">
                            <div class="panel panel-default">
                                <div class="panel-heading">Promoteur #{{ $promoteur->id }}</div>
                                <div class="panel-body">

                                    <a href="{{ url('/admin/promoteur') }}" title="Back"><button class="btn btn-warning btn-xs mt20"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                                    <a href="{{ url('/admin/promoteur/' . $promoteur->id . '/edit') }}" title="Edit Promoteur"><button class="btn btn-primary btn-xs mt20"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['admin/promoteur', $promoteur->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs mt20',
                                                'title' => 'Suppression Promoteur',
                                                'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                        ))!!}
                                    {!! Form::close() !!}
                                    <br/>
                                    <br/>

                                    <div class="table-responsive">
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <th>ID</th><td>{{ $promoteur->id }}</td>
                                                </tr>
                                                <tr><th> {{ trans('promoteur.denomination') }} </th><td> {{ $promoteur->denomination }} </td></tr><tr><th> {{ trans('promoteur.adresse') }} </th><td> {{ $promoteur->adresse }} </td></tr><tr><th> {{ trans('promoteur.telephone') }} </th><td> {{ $promoteur->telephone }} </td></tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                     </div>
            </div>
        </div>
    </div>
@endsection
