@extends('layouts.web')

@section('content')
     <div class="container">
                 <div class="row">
                    <div class="col-sm-12 ">
                        <ol class="breadcrumb mb30">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Accueil </a></li>
                            <li class="breadcrumb-item"><a href="{{url('/administration')}}">Administration </a></li>
                            <li class="breadcrumb-item"> <a href="{{ url('/admin/promoteur/') }}">Promoteur</a></li>
                            <li class="breadcrumb-item"> Edition</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                   
                    @include('admin.sidebar')
                    
                    <div class="col-md-10">
                        <div class="content_main rond3">
                        <div class="panel panel-default">
                                <div class="panel-heading">Edition :  Promoteur #{{ $promoteur->id }}</div>
                                <div class="panel-body">
                                    <a href="{{ url('/admin/promoteur') }}" title="Back"><button class="btn btn-warning btn-xs mt20"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                                    <br />
                                    <br />

                                    @if ($errors->any())
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif

                                    {!! Form::model($promoteur, [
                                        'method' => 'PATCH',
                                        'url' => ['/admin/promoteur', $promoteur->id],
                                        'class' => 'form-horizontal',
                                        'files' => true
                                    ]) !!}

                                    @include ('admin.promoteur.form', ['submitButtonText' => 'Update'])

                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
