<?php

return [
    'nom' => 'Nom',
'prenom' => 'Prénom',
'photo' => 'Photo',
'telephone' => 'Téléphone',
'sexe' => 'Sexe',
'biographie' => 'Biographie',
'adresse' => 'Adresse',
'date_nais' => 'Date de naissance',
'email' => 'Email',
'universite_id' => 'Université',
'titre_id' => 'Titre',
'affiliation_id' => 'Affiliation (Entité)',
];
