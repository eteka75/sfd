<?php

return [
    'denomination' => 'Denomination',
'adresse' => 'Adresse',
'telephone' => 'Telephone',
'email' => 'Email',
'datecreation' => 'Datecreation',
'ville' => 'Ville',
'assuranceentreprise' => 'Assuranceentreprise',
'licence' => 'Licence',
'type_id' => 'Type Id',
'surnom' => 'Surnom',
'nom' => 'Nom',
'prenom' => 'Prenom',
'sexe' => 'Sexe',
'situationmatrimoniale' => 'Situationmatrimoniale',
'nompere' => 'Nompere',
'nommere' => 'Nommere',
'niveau_etude' => 'Niveau Etude',
'nb_enfant_a_charge' => 'Nb Enfant A Charge',
'nb_autrepersone_a_charge' => 'Nb Autrepersone A Charge',
'etat' => 'Etat',
];
